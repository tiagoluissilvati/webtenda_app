import 'dart:convert';
import 'package:AppoioLab/dto/outDTO.dart';
import 'package:AppoioLab/models/ordenservico.dart';
import 'package:AppoioLab/models/tendas.dart';
import 'package:http/http.dart' as http;

class HttpService {
  //final String  URL = "https://20.197.188.130:8080/appoiolab/";
   final String  URL = "https://192.168.0.5:8080/appoiolab/";

  Future<List<Tenda>>  getTesdas() async {
    http.Response res = await http.get(Uri.parse(URL  + "tendas"));

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(utf8.decode(res.bodyBytes));

      List<Tenda> tendas = body
          .map(
            (dynamic item) => Tenda.fromJson(item),
      )
          .toList();

      return tendas;
    } else {
      throw "Unable to retrieve tendas.";
    }
  }


  Future<http.Response> login(String login, String senha) {
    return http.post(
      Uri.parse(URL  + "login"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'email': login,
        'senha': senha
      }),
    );
  }


  Future<http.Response> recuperarSenha(String login, String documento) {
    return http.post(
      Uri.parse(URL  + "recuperar"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'email': login,
        'documento': documento
      }),
    );
  }

  Future<OutDTO> cadastrar(String nome, String documento, String sexo, String dtnac, String email, String celular, String senha) async {

    Map<String, String> headers = {
      'Content-Type': 'application/json;charset=UTF-8',
      'Charset': 'utf-8',
      'Accept' : 'application/json; charset=UTF-8'
    };
     http.Response  res = await http.post(
      Uri.parse(URL  + "usuario"),
      headers: headers,
      body: jsonEncode(<String, String>{
        'nome': nome,
        'documento': documento,
        'sexo': sexo,
        'dtnac': dtnac,
        'email': email,
        'celular': celular,
        'senha': senha
      }),
    );

    OutDTO out = OutDTO.fromJson(json.decode(utf8.decode(res.bodyBytes))) ;

    return out;
  }


  Future<http.Response> ordenServico(OrdenServico os) {
    return http.post(
      Uri.parse(URL  + "ordemservico"),


      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, Object>{
        'idCliente': os.idCliente.toString(),
        'tendaId': os.tendaId.toString(),
        'data': os.data.toString(),
        'hora': os.hora.toString(),
        'pcr': os.pcr.toString(),
        'antigeno':  os.antigeno.toString()
      }),
    );
  }


  Future< List<OrdenServico>> getAgendamentos(String idCliente) async {
    http.Response res =  await http.post(
      Uri.parse(URL  + "consultar/ordemservico"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'idCliente': idCliente
      }),
    );

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(utf8.decode(res.bodyBytes));

      List<OrdenServico> ordens = body
          .map(
            (dynamic item) => OrdenServico.fromJson(item),
      )
          .toList();

      return ordens;
    } else {
      throw "Unable to retrieve ordens.";
    }



  }
}