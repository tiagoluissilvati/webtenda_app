
import 'package:AppoioLab/dto/mensagens.dart';

class OutDTO {
  late bool? success;
  late Mensagens? mensagens;

  OutDTO({ this.success = true, this.mensagens}){}

  factory OutDTO.fromJson(Map<String, dynamic> parsedJson) {

    return OutDTO(
        success: parsedJson['success'] as bool,
         mensagens:  Mensagens.fromJson(parsedJson['mensagens'] )
    );
  }
}