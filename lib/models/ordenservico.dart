
class OrdenServico {
  late int? idCliente;
  late int? tendaId;
  late String? tendaDs;

  late String? data;
  late String? hora;
  late bool? pcr;
  late bool? antigeno;


  late String? url;

  OrdenServico({this.tendaDs, this.data, this.url}){}

  factory OrdenServico.fromJson(Map<String, dynamic> parsedJson) {
    return OrdenServico(
        tendaDs: parsedJson['tendaDs'] as String,
        data: parsedJson['data']  as String,
        url: parsedJson['url']  as String
    );
  }
}