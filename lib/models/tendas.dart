
class Tenda implements Comparable<Tenda> {
  int idTenda;
  String descricao;

  Tenda({required this.idTenda, required this.descricao});


  factory Tenda.fromJson(Map<String, dynamic> parsedJson) {
    return Tenda(
        idTenda: parsedJson['idTenda'] as int,
        descricao: parsedJson['descricao']  as String
    );
  }

  @override
  String toString() {
    return descricao;
  }
  @override
  int compareTo(Tenda other) {

    return descricao.compareTo(other.descricao);
  }
}
