import 'package:AppoioLab/models/ordenservico.dart';
import 'package:AppoioLab/pages/menus.dart';
import 'package:flutter/material.dart';

class Confirmacao extends StatefulWidget {
  const Confirmacao({Key? key, required this.os}) : super(key: key);

  final OrdenServico os;

  @override
  State<Confirmacao> createState() => _ConfirmacaoState();
}

class _ConfirmacaoState extends State<Confirmacao> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
          title: Image.asset(
            'assets/images/logo.jpg',
            width: 150,
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          )),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/check_success.png',
            width: 150,
          ),
          Text("Agendamento Realizado com sucesso"),
          const SizedBox(height: 20,),
          Text("Local: " + (widget.os == null  || widget.os.tendaDs  == null ? '' : widget.os.tendaDs!)),
          Text("Horario: " + (widget.os == null  || widget.os.data  == null ? '' : widget.os.data!) + " " + widget.os.hora!),
          Container(
            padding: const EdgeInsets.only(top: 10),
            child: Align(
              alignment: Alignment.center,
              child: ElevatedButton(
                onPressed: () {
                  int? clienteID =    widget.os.idCliente;
                  OrdenServico os = OrdenServico();
                  os.idCliente = clienteID;

                  Route route = MaterialPageRoute(builder: (context) => Menus(os: os));
                  Navigator.pushReplacement(context, route);
                },
                style: ElevatedButton.styleFrom(
                    fixedSize: const Size(350, 75),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50))),
                child: const Text(
                  "Confirmar",
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
