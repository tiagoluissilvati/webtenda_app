import 'package:AppoioLab/models/ordenservico.dart';
import 'package:AppoioLab/models/tendas.dart';
import 'package:AppoioLab/pages/exames.dart';
import 'package:AppoioLab/services/http-services.dart';
import 'package:AppoioLab/utils/slide_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:time_picker_widget/time_picker_widget.dart';

class Agendamento extends StatefulWidget {
  const Agendamento({Key? key, required this.os}) : super(key: key);

  final OrdenServico os;

  @override
  State<Agendamento> createState() => _AgendamentoState();
}

class _AgendamentoState extends State<Agendamento> {
  TextEditingController controller = new TextEditingController();
  final HttpService httpService = HttpService();
  late TypeAheadField searchTextField;
  TimeOfDay selectedTime = TimeOfDay.now();
  String tendaDs = '';
  List<Tenda> tendas = [];
  Tenda selectedTenda =  Tenda(idTenda: 0, descricao: '');

  final FocusNode _searchNode = FocusNode(debugLabel: 'SearchField');
  GlobalKey key = new GlobalKey();

  final List<int> _availableMinutes = [0, 10, 20,  30, 40, 50];

  initState() {
    getTendas();
    selectedTime = TimeOfDay(hour: selectedTime.hour, minute: getMinute(selectedTime.minute) );
  }

  getMinute(int min) {
    if (min > 0 && min  < 5) {
      return 0;
    }
    if (min > 5 && min  < 10) {
      return 10;
    }
    if (min > 10 && min  < 20) {
      return 20;
    }
    if (min > 20 && min  < 30) {
      return 30;
    }
    if (min > 30 && min  < 40) {
      return 40;
    }
    if (min > 40 && min  < 50) {
      return 50;
    }
    return 0;
  }

  setSelectedTime(selected) {
    selectedTime = selected;
    print(selectedTime);
  }

  getTendas() async {
    var tendasRet = await httpService.getTesdas();

    tendasRet.forEach((t) {

      tendas.add(Tenda(idTenda : t.idTenda,  descricao: t.descricao));
    });
     return tendas;
  }


  Future pickTime(BuildContext context) async {
      print(selectedTime);
      if (selectedTime == null) {
        selectedTime = TimeOfDay.now();
        selectedTime = TimeOfDay(hour: selectedTime.hour, minute: getMinute(selectedTime.minute) );
      }

      final newTime = await  showCustomTimePicker(
          context: context,
          // It is a must if you provide selectableTimePredicate
          onFailValidation: (context) =>
              print( 'Unavailable selection.'),

          initialTime: selectedTime,
          selectableTimePredicate: (time) =>
          _availableMinutes.contains(time!.minute) ).then((time) => setSelectedTime(time));

      widget.os.hora = selectedTime.hour.toString() + ":" +selectedTime.minute.toString();

      widget.os.data= selectedDate.day.toString().padLeft(2, '0') + "/" + selectedDate.month.toString().padLeft(2, '0') + "/"  + selectedDate.year.toString();
      widget.os.tendaId = selectedTenda.idTenda;
      widget.os.tendaDs = selectedTenda.descricao;

      Navigator.push(context, SlideRoute( enterPage:  Exames(os: widget.os)));

  }

  clearTenda() {
    searchTextField.textFieldConfiguration.controller?.clear();
    selectedTenda  = Tenda(idTenda: 0, descricao: '');
    FocusScope.of(context).requestFocus(_searchNode);
  }

  DateTime selectedDate = DateTime.now();

  String removeDiacritics(String str) {

    var withDia = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var withoutDia = 'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz';

    for (int i = 0; i < withDia.length; i++) {
      str = str.replaceAll(withDia[i], withoutDia[i]);
    }

    return str;

  }
  submitAgenda() {
    if (selectedTenda.idTenda == 0) {
      print("1");
    } else {
      pickTime(context);
    }

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
          title: Image.asset(
            'assets/images/logo.jpg',
            width: 150,
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          )),

      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Container(
              width: 350,
              padding: const EdgeInsets.only(top: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Local"  , style: const TextStyle(fontWeight: FontWeight.bold),),
                  const Padding(padding:  EdgeInsets.only(bottom: 10)),

                  searchTextField =  TypeAheadField<Tenda>(
                      key: key,
                      suggestionsCallback: (pattern) {
                        List<Tenda> matches = <Tenda>[];
                        matches.addAll(tendas);
                        matches.retainWhere((s) =>  removeDiacritics(s.descricao)
                            .toLowerCase()
                            .contains(pattern.toLowerCase()));
                        return matches;
                      },

                      itemBuilder:  (context, item) {
                        return   Container(
                          padding: const EdgeInsets.only(top: 20),
                          child: Row(

                              children:  <Widget>[
                                Expanded(child: Text(item.descricao , style:  TextStyle(fontSize: 20),)),
                              ]

                          ),
                        );
                      },
                      onSuggestionSelected: (Tenda? suggestion) {
                        setState(() {
                          selectedTenda = suggestion!;
                        });
                      }),
          ])
        ),

          CalendarDatePicker(
              initialDate: DateTime.now(),
              firstDate: DateTime.now(),
              lastDate: DateTime(2099, 12, 12),
              onDateChanged: (date) {
                setState(() {
                  selectedDate = date;
                });
              }),

          Padding(padding: EdgeInsets.only(top: 20)),
          Container(
            width: 350,
            padding: const EdgeInsets.only(top: 20),
            child: Row(children: [

              const Text("Local:   ", style: const TextStyle(fontSize: 20)),
              Expanded(child: Text(selectedTenda.descricao, style: const TextStyle(fontSize: 20)))],
            ),
          ),

          Padding(padding: EdgeInsets.only(bottom: 30)),
          Container(
            padding: const EdgeInsets.only(top: 10),
            child: Align(
              alignment: Alignment.center,
              child: ElevatedButton(
                onPressed: () {
                    submitAgenda();
                },
                style: ElevatedButton.styleFrom(
                    fixedSize: const Size(350, 75),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50))),
                child: const Text(
                  "Confirmar",
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
