import 'package:AppoioLab/models/ordenservico.dart';
import 'package:AppoioLab/pages/agendamento.dart';
import 'package:AppoioLab/pages/login.dart';
import 'package:AppoioLab/pages/meusagendamentos.dart';
import 'package:AppoioLab/utils/slide_route.dart';
import 'package:flutter/material.dart';

class Menus extends StatefulWidget {


  const Menus({Key? key, required this.os}) : super(key: key);

  final OrdenServico os;

  @override
  State<Menus> createState() => _MenusState();
}

class _MenusState extends State<Menus> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          appBar: AppBar(title: Image.asset('assets/images/logo.jpg' , width: 150,), backgroundColor: Colors.white,
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Route route = MaterialPageRoute(builder: (context) =>  const Login(title: 'AppoioLab'));
                    Navigator.pushReplacement(context, route);
                  },
                  child: const Text('Sair'),
                )
              ] ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [

              Container(

                child: Align(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    onPressed: () {
                      //       Route route = MaterialPageRoute(builder: (context) => const Agendamento());
                      Navigator.push(context, SlideRoute( enterPage: Agendamento(os: widget.os)));
                    },
                    style: ElevatedButton.styleFrom(
                        fixedSize: const Size(350, 75),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50))),
                    child: const Text(
                      "Novo Agendamento",
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10),
                child: Align(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    onPressed: () {

                      Navigator.push(context, SlideRoute( enterPage:  MeusAgendamentos(os: widget.os)));
                    },
                    style: ElevatedButton.styleFrom(
                        fixedSize: const Size(350, 75),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50))),
                    child: const Text(
                      "Meus Agendamentos",
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ),
              ),
            ],),
        )
    );
  }
}
