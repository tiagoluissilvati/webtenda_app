import 'package:AppoioLab/models/ordenservico.dart';
import 'package:AppoioLab/services/http-services.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class MeusAgendamentos extends StatefulWidget {
  const MeusAgendamentos({Key? key, required this.os}) : super(key: key);

  final OrdenServico os;

  @override
  State<MeusAgendamentos> createState() => _MeusAgendamentosState();
}

class _MeusAgendamentosState extends State<MeusAgendamentos> {
  _DataSource ds =  _DataSource();

  final HttpService httpService = HttpService();
  @override
  void initState() {

    super.initState();
    initDS();
  }
  void initDS() async{
    _DataSource dsAux =  _DataSource();
    var agendamentos = await httpService.getAgendamentos(widget.os.idCliente.toString());
    int i = 0;
    agendamentos.forEach((t) {

      dsAux.add(_Row( t.tendaDs!,  t.data!,  t.url!,  i));
      i++;

    });
    setState(() {
      ds = dsAux;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
                title: Image.asset(
                  'assets/images/logo.jpg',
                  width: 150,
                ),
                backgroundColor: Colors.white,
                leading: IconButton(
                  icon: const Icon(Icons.arrow_back, color: Colors.black),
                  onPressed: () => Navigator.of(context).pop(),
                )),
            body: ListView(
              padding: const EdgeInsets.all(16),
                children: [

                        PaginatedDataTable(
                        header: const Text('Meus Agendamentos'),
                        showCheckboxColumn: false,
                        rowsPerPage: 10,
                        dataRowHeight: 100,
                        columns: const [
                        DataColumn(label: Text('Local')),
                        DataColumn(label: Text('Data')),
                        DataColumn(label: Text('')),
                      ], source: ds,
                        )
                ]
            )));
  }
}


class _Row {
  _Row(
      this.valueA,
      this.valueB,
      this.valueC,
      this.valueD,
      );

  final String valueA;
  final String valueB;
  final String valueC;
  final int valueD;

  bool selected = false;
}

class _DataSource extends DataTableSource {
  _DataSource() {
    _rows = <_Row>[
    ];
  }

  List<_Row> _rows = [];

  int _selectedCount = 0;

  @override
  DataRow? getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,

      cells: [
        DataCell(Container(width: 100, child: Text(row.valueA)) ),
        DataCell(Container(width: 80, child: Text(row.valueB ))),
        DataCell(Container(width: 30,
            child: InkWell(
              onTap: () {launchURL("https://apoiolablaudos.avantix.com.br/apoiolab/");}, // Image tapped
              splashColor: Colors.white10, // Splash color over image
              child: Ink.image(
                fit: BoxFit.cover, // Fixes border issues
                width: 30,
                height: 30,
                image: AssetImage(
                    'assets/images/relatorio.png'
                ),
              ),
            ))),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;

  void add(rowToAdd) {
    _rows.add(rowToAdd);
  }


  launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }
}
