import 'dart:io';
import 'package:AppoioLab/models/ordenservico.dart';
import 'package:AppoioLab/pages/cadastro.dart';
import 'package:AppoioLab/pages/menus.dart';
import 'package:AppoioLab/services/http-services.dart';
import 'package:AppoioLab/utils/slide_route.dart';
import 'package:flutter/material.dart';
import 'dart:convert' as convert;

class Login extends StatefulWidget {
  const Login({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _obscureText = true;
  OrdenServico os = OrdenServico();
  final HttpService httpService = HttpService();
  String login = '';
  String senha = '';
  String documento = '';



  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
  recueparSenha()  {
    login = '';
    documento = '';
    // set up the button
    Widget okButton = TextButton(
      child: Text("Confirmar"),
      onPressed: () async {

        if (login.isNotEmpty  || documento.isNotEmpty ) {
          await httpService.recuperarSenha(login, documento);


          Navigator.of(context, rootNavigator: true)
              .pop();

          // set up the button
          Widget okConfirmButton = TextButton(
            child: Text("OK"),
            onPressed: () {
              Navigator.of(context, rootNavigator: true)
                  .pop();
            },
          );

          // set up the AlertDialog
          AlertDialog alert = AlertDialog(
            title: Text("Sucesso"),
            content: Text("Sua senha foi enviada para seu email com sucesso"),
            actions: [
              okConfirmButton,
            ],
          );

          // show the dialog
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return alert;
              },
              barrierDismissible: false
          );



        }
      },
    );
    // set up the button
    Widget cancelarButton = TextButton(
      child: Text("Cancelar"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true)
            .pop();
      },
    );

    Widget conteudo = SingleChildScrollView(child: Column(
        children: <Widget>[
          Container(
            width: 350,
            padding: const EdgeInsets.only(top: 20),
            child:  TextField(
              autofocus: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'E-mail',
              ),

              onChanged: (String value) {
                login = value;
              },
              textInputAction: TextInputAction.next,
            ),
          ),
          const Padding(padding: EdgeInsets.only(top: 10)),

          Text("OU", style: TextStyle(fontSize: 15),),

          Container(
            width: 350,
            padding: const EdgeInsets.only(top: 20),
            child:  TextField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'CPF / Passaporte',
              ),

              onChanged: (String value) {
                login = value;
              },
              textInputAction: TextInputAction.next,
            ),
          ),
        ]
    ));

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Recuperar Senha "),

      insetPadding: EdgeInsets.symmetric(vertical: 100),
      content: conteudo,
      actions: [
        cancelarButton,
        okButton,
      ],
    );

    // show the dialog
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
        barrierDismissible: false
    );
  }
  void efetuarLogin() async{
    try {
      final response =   await httpService.login(login, senha);
      if (response.statusCode != 200) throw HttpException('${response.statusCode}');
      final jsonMap = convert.jsonDecode(response.body);

      print(jsonMap);

      os.idCliente = jsonMap['idUsuario'] as int;
       Route route = MaterialPageRoute(builder: (context) => Menus(os: os,));
       Navigator.pushReplacement(context, route);
    } on SocketException {
      print('No Internet connection 😑');
    } on HttpException {
      print("Couldn't find the post 😱");
    } on FormatException {
      print("Bad response format 👎");


      // set up the button
      Widget okButton = TextButton(
        child: Text("OK"),
        onPressed: () {
          Navigator.of(context, rootNavigator: true)
              .pop();
          },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text("LOGIN  "),
        content: Text("E-mail / Senha inválidos"),
        actions: [
          okButton,
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
        barrierDismissible: false
      );

    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60),
              child: Image.asset('assets/images/logo.jpg'),
            ),
            Container(
              width: 350,
              padding: const EdgeInsets.only(top: 20),
              child:  TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'E-mail',
                ),

                onChanged: (String value) {
                  login = value;
                },
                textInputAction: TextInputAction.next,
              ),
            ),
            Container(
              width: 350,
              padding: const EdgeInsets.only(top: 20),
              child: TextFormField(
                  obscureText: _obscureText,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                      suffixIcon: IconButton(
                        icon: Icon(Icons.lock),
                        onPressed: _toggle,
                      )),

                onChanged: (String value) {
                  senha = value;
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.only(right: 18),
              child: Align(
                alignment: Alignment.centerRight,
                child: TextButton(
                  onPressed: () {
                     recueparSenha();
                  },
                  child: Text("Recuperar Senha"),
                ),
              ),
            ),
            Container(
              child: Align(
                alignment: Alignment.center,
                child: ElevatedButton(
                  onPressed: () {
                    OrdenServico os = OrdenServico();

                    efetuarLogin();


                  },
                  style: ElevatedButton.styleFrom(
                      fixedSize: const Size(350, 75),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50))),
                  child: const Text(
                    "Login",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(top: 10),
              child: Align(
                alignment: Alignment.center,
                child: ElevatedButton(
                  onPressed: () {

                    Navigator.push(context, SlideRoute( enterPage: const Cadastro()));
                  },
                  style: ElevatedButton.styleFrom(
                      fixedSize: const Size(350, 75),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50))),
                  child: const Text(
                    "Cadastrar",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            )
          ],
        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
}
