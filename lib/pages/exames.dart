import 'package:AppoioLab/models/ordenservico.dart';
import 'package:AppoioLab/pages/confirmacao.dart';
import 'package:AppoioLab/services/http-services.dart';
import 'package:flutter/material.dart';
import 'dart:io';

class Exames extends StatefulWidget {
  const Exames({Key? key, required this.os}) : super(key: key);

  final OrdenServico os;

  @override
  State<Exames> createState() => _ExamesState();
}

class _ExamesState extends State<Exames> {
  bool isChecked = false;
  bool checkPCR = false;
  bool checkAntigeno = false;
  DateTime selectedDate = DateTime.now();

  HttpService httpService = HttpService();


  executaConfirmar() {

    // set up the button
    Widget okConfirmButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true)
            .pop();

        Route route = MaterialPageRoute(builder: (context) => Confirmacao(os: widget.os));
        Navigator.pushReplacement(context, route);
      },
    );
    Widget okCancelButton = TextButton(
      child: Text("Cancelar"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true)
            .pop();

      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(

      title: Text("Confirmação"),
      content: Container(

        height: 200.0,
        width: 400.0,
        child: Column(children: [

          Row(children : [Flexible(child: Text("Local: " + (widget.os == null  || widget.os.tendaDs  == null ? '' : widget.os.tendaDs!)))]),
          SizedBox(height: 20,),
          Row(children : [Text("Horario: " + (widget.os == null  || widget.os.data  == null ? '' : widget.os.data!) + " " + widget.os.hora!)]),

        ],),
      ),
      actions: [
        okConfirmButton,
        okCancelButton
      ],
    );

    // show the dialog
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
        barrierDismissible: false
    );
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
          title: Image.asset(
            'assets/images/logo.jpg',
            width: 150,
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          )),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            "             RT-PCR",
            style: TextStyle(fontSize: 25),
          ),
          Container(
              child: Align(
            child: Column(children: [
              Row(children: [
                Padding(padding: EdgeInsets.only(left: 30),),
                Transform.scale(
                    scale: 2.0,
                    child: Checkbox(
                      activeColor: const Color(0xFF2850ff),
                      checkColor: Colors.white,
                      value: checkPCR,
                      onChanged: (bool? value) {
                        setState(() {
                          checkPCR = value!;
                        });
                      },
                    )),
                Image.asset(
                  'assets/images/RPC.jpeg',
                  width: 200,
                  fit: BoxFit.contain,
                )
              ]),
              const Text(
                "       ANTIGENO",
                style: TextStyle(fontSize: 25),
              ),
              Row(children: [
                Padding(padding: EdgeInsets.only(left: 30),),
                Transform.scale(
                    scale: 2.0,
                    child: Checkbox(
                      activeColor: const Color(0xFF2850ff),
                      checkColor: Colors.white,
                      value: checkAntigeno,
                      onChanged: (bool? value) {
                        setState(() {
                          checkAntigeno = value!;
                        });
                      },
                    )),
                Image.asset(
                  'assets/images/ANTIGENO.jpeg',
                  width: 200,
                  fit: BoxFit.contain,
                )
              ])
            ]),
          )),
          Padding(padding: EdgeInsets.only(top: 15),),
          Container(

            child: Align(
              alignment: Alignment.center,
              child: ElevatedButton(
                onPressed: () async {
                  widget.os.pcr = checkPCR;
                  widget.os.antigeno = checkAntigeno;

                  final response = await  httpService.ordenServico(widget.os);
                  if (response.statusCode != 200) throw HttpException('${response.statusCode}');

                  executaConfirmar();
                },
                style: ElevatedButton.styleFrom(
                    fixedSize: const Size(350, 75),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50))),
                child: const Text(
                  "Confirmar",
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
          )
        ],
      ),
    ));
  }
}
