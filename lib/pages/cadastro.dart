import 'package:AppoioLab/dto/outDTO.dart';
import 'package:AppoioLab/services/http-services.dart';
import 'package:AppoioLab/utils/datehelper.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';


class Cadastro extends StatefulWidget {
  const Cadastro({Key? key}) : super(key: key);

  @override
  State<Cadastro> createState() => _CadastroState();
}

class _CadastroState extends State<Cadastro> {
  bool erro = false;
  String msg = "";
  late String _nome;
  late String _documento;
  late String _sexo = "M";
  late String _dtnac;
  DateTime _dtnacSelect = DateTime.now();
  late String _email;
  late String _celular;
  late String _senha;
  late String _senhaConfimar;

  final HttpService httpService = HttpService();
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();


  final dateHolder = TextEditingController();

  Widget _createNome() {
    return TextFormField(
      autofocus: true,
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(labelText: "Nome",),
      validator: (String? value) {
        if (value != null && value.isEmpty) {
          return "Nome é obrigatorio";
        }
      },
      onSaved: (String? value) {
        _nome = value!;
      },
    );
  }

  Widget _createDocumento() {
    return TextFormField(
      decoration: InputDecoration(labelText: "CPF/Passaporte",),
      textInputAction: TextInputAction.next,
      validator: (String? value) {
        if (value != null && value.isEmpty) {
          return "Documento é obrigatorio";
        }
      },
      onSaved: (String? value) {
        _documento = value!;
      },
    );
  }

  Widget _createSexo() {
    return Column(
      children: [
        ListTile(
          title: const Text('Masculino'),
          leading: Radio<String>(
            value: "M",
            groupValue: _sexo,
            onChanged: (String? value) {
              setState(() {
                _sexo = value!;
              });
            },
          ),
        ),
        ListTile(
          title: const Text('Feminino'),
          leading: Radio<String>(
            value: "F",
            groupValue: _sexo,
            onChanged: (String? value) {
              setState(() {
                _sexo = value!;
              });
            },
          ),
        ),
      ],
    );
  }

  Widget _createDtNac() {
    var maskFormatter = MaskTextInputFormatter(
        mask: '##/##/####',
        filter: { "#": RegExp(r'[0-9]')},
        type: MaskAutoCompletionType.lazy
    );
    return TextFormField(

      controller: dateHolder,
      textInputAction: TextInputAction.next,
      inputFormatters: [maskFormatter],
      keyboardType: TextInputType.datetime,
      decoration: InputDecoration(labelText: "Data Nacimento",),
      validator: (String? value) {
        if (value != null && value.isEmpty) {
          return "Data Nacimento é obrigatorio";
        }
        if (!DateHelper.isValidDateBirth(value!, "dd/MM/yyyy")) {
          return "Data Nacimento inválida";
        }
      },
        onTap: () async{
      DateTime? date = DateTime(1900);
      FocusScope.of(context).requestFocus(new FocusNode());

      date = await showDatePicker(context: context,
          initialDate:DateTime.now(),
          firstDate:DateTime(1900),
          lastDate: DateTime(2100));
      setState(() {
        dateHolder.text = DateFormat('dd/MM/yyyy').format(date!);
      });

      },
      onSaved: (String? value) {
        _dtnac = value!;
      },
    );
  }

  Widget _createEmail() {
    return TextFormField(
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(labelText: "E-mail",),
      validator: (String? value) {
        if (value != null && value.isEmpty) {
          return "E-mail é obrigatorio";
        }
        bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
            .hasMatch(value!);
        if (!emailValid) {
          return "E-mail é inválido";
        }
      },
      onSaved: (String? value) {
        _email = value!;
      },
    );
  }

  Widget _createCelular() {
    var maskFormatter = MaskTextInputFormatter(
        mask: '(##)#####-####',
        filter: { "#": RegExp(r'[0-9]')},
        type: MaskAutoCompletionType.lazy
    );
    return TextFormField(
      textInputAction: TextInputAction.done,
      inputFormatters: [maskFormatter],
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(labelText: "Celular",),

      onSaved: (String? value) {
        _celular = value!;
      },
    );
  }

  Widget _createSenha() {
    return Container();
  }

  void efetivarCadastro() async {
      try {
         msg = "Seu cadastro foi realizado com Sucesso, efetue o login";

        OutDTO response = await httpService.cadastrar(
            _nome,
            _documento,
            _sexo,
            _dtnac,
            _email,
            _celular,
            _senha);
        erro = !response.success!;
        if (erro) {
          msg = "Usuário já Cadastrado ! Recupere a sua senha para realizar o Login";
        }
      } catch (e) {
        print(e);
      }


      // set up the button
      Widget okButton = TextButton(
        child: Text("OK"),
        onPressed: () {
          Navigator.of(context, rootNavigator: true)
              .pop();
          if (!erro) {

            Navigator.of(context, rootNavigator: true)
                .pop();
          }
        },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text("Cadastro  "),
        content: Text(msg),
        actions: [
          okButton,
        ],
      );

      // show the dialog
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
          barrierDismissible: false
      );
}


  selecionarSenha()  {
    _senha = '';
    _senhaConfimar = '';
    // set up the button
    Widget okButton = TextButton(
      child: Text("Confirmar"),
      onPressed: () async {

        if (!_formKey2.currentState!.validate()) {
          return;
        }
        efetivarCadastro();
        Navigator.of(context, rootNavigator: true)
            .pop();

      },
    );
    // set up the button
    Widget cancelarButton = TextButton(
      child: Text("Cancelar"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true)
            .pop();
      },
    );

    Widget conteudo = SingleChildScrollView(child: Form(
      key: _formKey2,
      child: Column(
          children: <Widget>[
            Container(
              width: 350,
              padding: const EdgeInsets.only(top: 20),
              child:  TextFormField(
                autofocus: true,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Senha',
                ),

                onSaved : (String? value) {
                  _senha = value!;
                },
                textInputAction: TextInputAction.next,
                validator: (String? value) {
                    _senha = value!;
                    if (value != null && value.isEmpty) {
                      return "Senha é obrigatorio";
                    }
                }
              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 10)),

            Container(
              width: 350,
              padding: const EdgeInsets.only(top: 20),
              child:  TextFormField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Confirmar Senha',
                ),

                onChanged: (String value) {
                  _senhaConfimar = value;
                },
                textInputAction: TextInputAction.next,
                validator: (String? value) {
                    if (value != null && value.isEmpty) {
                      return "Confirmar Senha é obrigatorio";
                    }

                    print(value);
                    print(_senha);
                    if (value != _senha) {
                      return "As senhas devem ser iguais";

                    }
                  }
              ),
            ),
          ]
      ),
    ));

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Cadastrar Senha "),
      insetPadding: EdgeInsets.only(top: 10),
      content: conteudo,
      actions: [
        cancelarButton,
        okButton,
      ],
    );

    // show the dialog
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
        barrierDismissible: false
    );
  }

@override
Widget build(BuildContext context) {
  return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            title: Image.asset(
              'assets/images/logo.jpg',
              width: 150,
            ),
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: const Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.of(context).pop(),
            )),
        body: Container(
            width: 350,
            padding: const EdgeInsets.only(top: 20, left: 10),
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _createNome(),
                    _createDocumento(),
                    SizedBox(height: 20,),
                    Text("Sexo"),
                    _createSexo(),
                    _createDtNac(),
                    _createEmail(),
                    _createCelular(),
                    _createSenha(),
                    SizedBox(height: 100,),
                    ElevatedButton(
                      onPressed: () {
                        if (!_formKey.currentState!.validate()) {
                          return;
                        }
                        _formKey.currentState!.save();
                        selecionarSenha();
                      },
                      style: ElevatedButton.styleFrom(
                          fixedSize: const Size(350, 75),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50))),
                      child: const Text(
                        "Cadastrar",
                        style: TextStyle(fontSize: 20),
                      ),
                    )
                  ],
                ),
              ),
            )),
      ));
}}
